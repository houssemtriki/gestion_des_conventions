package com.examen.j2ee.examen.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.examen.j2ee.examen.entiter.Participant;
import com.examen.j2ee.examen.entiter.SearchForm;
import com.examen.j2ee.examen.entiter.TypeConv;
import com.examen.j2ee.examen.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
@RequestMapping("/participants")
public class ParticipantController {
	public static List<Participant> participants = new ArrayList<>();
	private TypeConv typeConv;
	private DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	@Autowired
	private ParticipantService participantService;

	@RequestMapping("/getAll")
	public String getAll(Model model) {
		List<Participant> participants = participantService.getAll();
		model.addAttribute("participants", participants);
		String username = "Kindson";
		model.addAttribute("username", username);
		return "participants";
	}

	@RequestMapping("/getOne")
	@ResponseBody
	public Participant getOne(Long idParticipant) {
		return participantService.getOne(idParticipant);
	}
	@PostMapping("/addNew")
	public String addNew(Participant participant) {
		participantService.addNew(participant);
		return "redirect:/participants/getAll";
	}
	@RequestMapping(value="/update", method = {RequestMethod.PUT, RequestMethod.GET})
	public String update(Participant participant) {
		participantService.update(participant);
		return "redirect:/participants/getAll";
	}
	@RequestMapping(value="/delete", method = {RequestMethod.DELETE, RequestMethod.GET})
	public String delete(Long idParticipant) {
		participantService.delete(idParticipant);
		return "redirect:/participants/getAll";
	}


}