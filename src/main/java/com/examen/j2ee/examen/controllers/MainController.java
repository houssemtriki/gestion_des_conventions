package com.examen.j2ee.examen.controllers;

import com.examen.j2ee.examen.entiter.Administrateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MainController  implements ErrorController {



    @RequestMapping("/")
    public String home(){
        return "redirect:/conventions/getAll";
    }
    @RequestMapping("/home/conventions/getAll")
    public String error() {
        return "Error handling";
    }

    @Override
    public String getErrorPath() {
        return "/home/conventions/getAll";
    }
}
