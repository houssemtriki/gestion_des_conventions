package com.examen.j2ee.examen.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.examen.j2ee.examen.entiter.*;
import com.examen.j2ee.examen.service.ConventionService;
import com.examen.j2ee.examen.service.ParticipantService;
import com.examen.j2ee.examen.service.SignatureService;
import com.examen.j2ee.examen.service.TypeConvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/conventions")
public class ConventionController {
	@Autowired
	private ConventionService conventionService;
	@Autowired
	private ParticipantService participantService;
	@Autowired
	private TypeConvService typeService;
	@Autowired
	private SignatureService signatureService;

	private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private static Administrateur admin;
	public static List<ConventionForm> conventionList = new ArrayList<>();



	@RequestMapping("/getAll")
	public String getAll(Model model, Authentication authentication) {
		AdminPrincipal adminPrincipal = (AdminPrincipal) authentication.getPrincipal();
		admin = adminPrincipal.getAdmin();
		model.addAttribute("admin", admin);
		model.addAttribute("participants", participantService.getAll());
		model.addAttribute("types", typeService.listAll());
		model.addAttribute("formater", formatter);
		model.addAttribute("conventions", conventionService.getAll());
		conventionList.clear();
		for (Convention convention: conventionService.getAll())
		{
			List<Signature> signatures = (List<Signature>) convention.getSignatures();
			ConventionForm conventionForm=new ConventionForm(convention,signatures);
			conventionList.add(conventionForm);
		}
		String username = "Admin";
		model.addAttribute("username", username);
		return "conventions";
	}

	@RequestMapping("/getConventionForm")
	@ResponseBody
	public ConventionForm getConventionForm(Long idConv){
		Convention convention = conventionService.getOne(idConv).get();
		List<Signature> signatures = (List<Signature>) convention.getSignatures();
		System.out.println(signatures.get(1).getDateSignature());
		return new ConventionForm(convention, signatures);

	}

	@RequestMapping("/getOne")
	@ResponseBody
	public Optional<Convention> getOne(Long idConv) {
		return conventionService.getOne(idConv);
	}


	@PostMapping("/addNew")
	public String addNew(ConventionForm conventionForm , Model model) throws ParseException {
		System.out.println(conventionForm);
		Convention convention = new Convention(conventionForm.getDateEdition(),
				conventionForm.getObjetConv(),
				conventionForm.getDateEntreeVigueur(),
				conventionForm.getDateExpiration(),
				typeService.get(conventionForm.getTypeConvention()),
				admin);

		convention = conventionService.addNew(convention);
		List<Signature> signatures = new ArrayList<>();
		Signature signature1 = new Signature(conventionForm.getDateSignature1(),
				participantService.getOne(conventionForm.getParticipant1()),
				convention);
		signatures.add(signature1);
		Signature signature2 = new Signature(conventionForm.getDateSignature2(),
				participantService.getOne(conventionForm.getParticipant2()),
				convention);
		signatures.add(signature2);
		if(!conventionForm.getDateSignature3().equals(""))
		{
			Signature signature3 = new Signature(conventionForm.getDateSignature3(),
					participantService.getOne(conventionForm.getParticipant3()),
					convention);
			signatures.add(signature3);
		}
		if(!conventionForm.getDateSignature4().equals(""))
		{
			Signature signature4 = new Signature(conventionForm.getDateSignature4(),
					participantService.getOne(conventionForm.getParticipant4()),
					convention);
			signatures.add(signature4);
		}
		for (Signature signa :
				signatures) {
			System.out.println(signa);
		}
		for (Signature signa :
				signatures) {
			signatureService.addNew(signa);
		}
		return "redirect:/conventions/getAll";
	}



	@RequestMapping(value="/update", method = {RequestMethod.POST})
	public String update(ConventionForm conventionForm )  throws ParseException {
		Convention convention = new Convention(conventionForm.getDateEdition(),
				conventionForm.getObjetConv(),
				conventionForm.getDateEntreeVigueur(),
				conventionForm.getDateExpiration(),
				typeService.get(conventionForm.getTypeConvention()),
				admin);
		convention.setIdConv(conventionForm.getIdConv());
		convention = conventionService.update(convention);
		signatureService.deleteAllSignaturesOfConvention(convention.getIdConv());
		List<Signature> signatures = new ArrayList<>();
		Signature signature1 = new Signature(conventionForm.getDateSignature1(),
				participantService.getOne(conventionForm.getParticipant1()),
				convention);
		signatures.add(signature1);
		Signature signature2 = new Signature(conventionForm.getDateSignature2(),
				participantService.getOne(conventionForm.getParticipant2()),
				convention);
		signatures.add(signature2);
		if (conventionForm.getParticipant3() != 0) {
			Signature signature3 = new Signature(conventionForm.getDateSignature3(),
					participantService.getOne(conventionForm.getParticipant3()),
					convention);
			signatures.add(signature3);
		}
		if (conventionForm.getParticipant4() != 0) {
			Signature signature4 = new Signature(conventionForm.getDateSignature4(),
					participantService.getOne(conventionForm.getParticipant4()),
					convention);
			signatures.add(signature4);
		}
		for (Signature signa :
				signatures) {
			System.out.println(signa);
		}
		for (Signature signa :
				signatures) {
			signatureService.update(signa);
		}
		return "redirect:/conventions/getAll";
	}

	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable(name = "id") Long idConv) {
		conventionService.delete(idConv);
		return "redirect:/conventions/getAll";
	}

	@RequestMapping("/pdf")
	public String pdfView(Model model){
		model.addAttribute("formatter", formatter);
		model.addAttribute("participantService", participantService);
		model.addAttribute("typeService", typeService);
		model.addAttribute("conventionList", conventionList);
		return "pdf";
	}
}