package com.examen.j2ee.examen.service;

import java.util.List;
import java.util.Optional;

import com.examen.j2ee.examen.dao.ParticipantRepository;
import com.examen.j2ee.examen.entiter.Participant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class ParticipantService {
	@Autowired
	private ParticipantRepository participantRepository;

	public List<Participant> getAll() {
		return (List<Participant>) participantRepository.findAll();
	}

	public Participant getOne(Long idParticipant) {
		return participantRepository.findById(idParticipant).get();
	}
	public void addNew(Participant participant) {
		participantRepository.save(participant);
	}

	public void update(Participant participant) {
		participantRepository.save(participant);
	}
	public void delete(Long idParticipant) {
		participantRepository.deleteById(idParticipant);
	}
}
