package com.examen.j2ee.examen.service;

import com.examen.j2ee.examen.dao.SignatureRepository;
import com.examen.j2ee.examen.entiter.Signature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SignatureService {
    @Autowired
    private SignatureRepository signatureRepository;

    public List<Signature> getAll() {
        return (List<Signature>) signatureRepository.findAll();
    }

    public Signature getOne(Long idSignature) {
        return signatureRepository.findById(idSignature).get();
    }

    public void addNew(Signature signature) {
        signatureRepository.save(signature);
    }

    public void update(Signature signature) {
        signatureRepository.save(signature);
    }

    public void delete(Long idSignature) {
        signatureRepository.deleteById(idSignature);
    }

    public void deleteAllSignaturesOfConvention(Long conventionId) {
        signatureRepository.deleteAllSignaturesOfConvention(conventionId);
    }
}
