package com.examen.j2ee.examen.service;

import com.examen.j2ee.examen.dao.TypeRepository;
import com.examen.j2ee.examen.entiter.TypeConv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeConvService {
    @Autowired
    private TypeRepository typeRepository;

    public List<TypeConv> listAll() {
        return typeRepository.findAll();
    }

    public void save(TypeConv product) {
        typeRepository.save(product);
    }

    public TypeConv get(Long id) {
        return typeRepository.getOne(id);
    }

    public void delete(Long id) {
        typeRepository.deleteById(id);
    }
}
