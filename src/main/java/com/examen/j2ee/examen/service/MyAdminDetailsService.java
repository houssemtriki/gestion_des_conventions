package com.examen.j2ee.examen.service;

import com.examen.j2ee.examen.dao.AdminRepository;
import com.examen.j2ee.examen.entiter.AdminPrincipal;
import com.examen.j2ee.examen.entiter.Administrateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class MyAdminDetailsService implements UserDetailsService {
	@Autowired
	private AdminRepository adminRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Administrateur admin = adminRepository.findByLogin(login);
		if(admin == null) {
			throw new UsernameNotFoundException("user not found");
		}
		return new AdminPrincipal(admin);
	}

}
