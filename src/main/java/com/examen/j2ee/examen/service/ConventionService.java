package com.examen.j2ee.examen.service;

import java.util.List;
import java.util.Optional;

import com.examen.j2ee.examen.dao.ConventionRepository;
import com.examen.j2ee.examen.entiter.Convention;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ConventionService {
	@Autowired
	private ConventionRepository conventionRepository;

	public List<Convention> getAll() {
		return (List<Convention>) conventionRepository.findAll();
	}

	public Optional<Convention> getOne(Long idConv) {
		return conventionRepository.findById(idConv);
	}

	public Convention addNew(Convention convention) {
		return conventionRepository.save(convention);
	}

	public Convention update(Convention convention) {
		conventionRepository.save(convention);
        return convention;
    }

	public void delete(Long idConv) {
		conventionRepository.deleteById(idConv);
	}
}
