package com.examen.j2ee.examen.dao;

import com.examen.j2ee.examen.entiter.TypeConv;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends JpaRepository<TypeConv, Long> {
}
