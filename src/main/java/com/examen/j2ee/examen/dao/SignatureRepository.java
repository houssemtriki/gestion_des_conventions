package com.examen.j2ee.examen.dao;

import com.examen.j2ee.examen.entiter.Signature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SignatureRepository extends JpaRepository<Signature,Long> {
    @Query(value = "delete  from signature where id_con_fk=:id_con_fk", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteAllSignaturesOfConvention(@Param("id_con_fk") Long conventionId);

}
