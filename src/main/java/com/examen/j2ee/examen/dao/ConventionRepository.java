package com.examen.j2ee.examen.dao;

import com.examen.j2ee.examen.entiter.Convention;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConventionRepository extends JpaRepository<Convention,Long > {
}
