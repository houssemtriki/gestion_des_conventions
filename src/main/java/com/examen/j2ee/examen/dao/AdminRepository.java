package com.examen.j2ee.examen.dao;

import com.examen.j2ee.examen.entiter.Administrateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AdminRepository extends JpaRepository<Administrateur, Long> {
	Administrateur findByLogin(String login);
}
