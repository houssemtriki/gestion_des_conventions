package com.examen.j2ee.examen.dao;

import com.examen.j2ee.examen.entiter.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipantRepository extends JpaRepository <Participant,Long> {
}
