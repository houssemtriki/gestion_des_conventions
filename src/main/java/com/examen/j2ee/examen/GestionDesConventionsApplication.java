package com.examen.j2ee.examen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class GestionDesConventionsApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext run = SpringApplication.run(GestionDesConventionsApplication.class, args);
    }

}
