package com.examen.j2ee.examen.entiter;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Participant implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idParticipant;
	@Column(nullable = false)
	private String designation;
//	@OneToMany(cascade = {CascadeType.MERGE},mappedBy = "participant",fetch = FetchType.LAZY)
//	private Collection<Signature> signatures;

	public Participant() {
		super();
	}

	public Participant(String designation) {
		super();
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Participant{" +
				"idParticipant=" + idParticipant +
				", designation='" + designation + '\'' +
				'}';
	}

	public Long getIdParticipant() {
		return idParticipant;
	}

	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((designation == null) ? 0 : designation.hashCode());
		result = prime * result + ((idParticipant == null) ? 0 : idParticipant.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participant other = (Participant) obj;
		if (designation == null) {
			if (other.designation != null)
				return false;
		} else if (!designation.equals(other.designation))
			return false;
		if (idParticipant == null) {
			if (other.idParticipant != null)
				return false;
		} else if (!idParticipant.equals(other.idParticipant))
			return false;
		return true;
	}

//	public Collection<Signature> getSignatures() {
//		return signatures;
//	}
//
//	public void setSignatures(Collection<Signature> signatures) {
//		this.signatures = signatures;
//	}

}
