package com.examen.j2ee.examen.entiter;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TypeConv implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTypConv;
	@Column(nullable = false)
	private String designation;
	@OneToMany(cascade = { CascadeType.MERGE }, mappedBy = "typeConvention", fetch = FetchType.LAZY)
	private Collection<Convention> conventions;

	public TypeConv() {
		super();
	}

	public TypeConv(String designation) {
		super();
		this.designation = designation;
	}

	public Collection<Convention> getConventions() {
		return conventions;
	}

	public void setConventions(Collection<Convention> conventions) {
		this.conventions = conventions;
	}

	public Long getIdTypConv() {
		return idTypConv;
	}

	public void setIdTypConv(Long idTypConv) {
		this.idTypConv = idTypConv;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conventions == null) ? 0 : conventions.hashCode());
		result = prime * result + ((designation == null) ? 0 : designation.hashCode());
		result = prime * result + ((idTypConv == null) ? 0 : idTypConv.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeConv other = (TypeConv) obj;
		if (conventions == null) {
			if (other.conventions != null)
				return false;
		} else if (!conventions.equals(other.conventions))
			return false;
		if (designation == null) {
			if (other.designation != null)
				return false;
		} else if (!designation.equals(other.designation))
			return false;
		if (idTypConv == null) {
			if (other.idTypConv != null)
				return false;
		} else if (!idTypConv.equals(other.idTypConv))
			return false;
		return true;
	}

}
