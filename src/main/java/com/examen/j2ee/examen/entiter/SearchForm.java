package com.examen.j2ee.examen.entiter;

public class SearchForm {
    private long idParticipant;
    private String designation;

    public SearchForm(long idParticipant, String designation) {
        this.idParticipant = idParticipant;
        this.designation = designation;
    }

    public long getIdParticipant() {
        return idParticipant;
    }

    public void setIdParticipant(long idParticipant) {
        this.idParticipant = idParticipant;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
}
