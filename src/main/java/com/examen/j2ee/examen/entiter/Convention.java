package com.examen.j2ee.examen.entiter;

import org.springframework.lang.NonNull;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;

@Entity
public class Convention implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idConv;
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date dateEdition;
	@Column(nullable = false)
	private String objetConv;
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date dateEntreeVigueur;
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date dateExpiration;

//	@OneToMany(cascade = { CascadeType.MERGE }, mappedBy = "convention", fetch = FetchType.LAZY)
//	private Collection<Signature> signatures;
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "idTypConv")
	private TypeConv typeConvention;
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "idAdmin")
	private Administrateur administrateur;
	@OneToMany(cascade = { CascadeType.MERGE }, mappedBy = "convention"  ,fetch = FetchType.LAZY)
	private Collection<Signature> signatures;


	public Convention() {
		super();
	}

	public Collection<Signature> getSignatures() {
		return signatures;
	}

	public void setSignatures(Collection<Signature> signatures) {
		this.signatures = signatures;
	}

	public Convention(Date dateEdition, String objetConv, Date dateEntreeVigueur, Date dateExpiration) {
		super();
		this.dateEdition = dateEdition;
		this.objetConv = objetConv;
		this.dateEntreeVigueur = dateEntreeVigueur;
		this.dateExpiration = dateExpiration;
	}

	public Convention(Date dateEdition, String objetConv, Date dateEntreeVigueur, Date dateExpiration,
			TypeConv typeConvention, Administrateur administrateur) {
		super();
		this.dateEdition = dateEdition;
		this.objetConv = objetConv;
		this.dateEntreeVigueur = dateEntreeVigueur;
		this.dateExpiration = dateExpiration;
		this.typeConvention = typeConvention;
		this.administrateur = administrateur;
	}
	public Convention(String dateEdition, String objetConv, String dateEntreeVigueur, String dateExpiration,
					  TypeConv typeConvention, Administrateur administrateur) throws ParseException {
		super();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		this.dateEdition = formatter.parse(dateEdition);
		this.objetConv = objetConv;
		this.dateEntreeVigueur = formatter.parse(dateEntreeVigueur);
		this.dateExpiration = formatter.parse(dateExpiration);
		this.typeConvention = typeConvention;
		this.administrateur = administrateur;
	}

	public Long getIdConv() {
		return idConv;
	}

	public void setIdConv(Long idConv) {
		this.idConv = idConv;
	}

	public Date getDateEdition() {
		return dateEdition;
	}

	public void setDateEdition(Date dateEdition) {
		this.dateEdition = dateEdition;
	}

	public String getObjetConv() {
		return objetConv;
	}

	public void setObjetConv(String objetConv) {
		this.objetConv = objetConv;
	}

	public Date getDateEntreeVigueur() {
		return dateEntreeVigueur;
	}

	public void setDateEntreeVigueur(Date dateEntreeVigueur) {
		this.dateEntreeVigueur = dateEntreeVigueur;
	}

	public Date getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

//	public Collection<Signature> getSignatures() {
//		return signatures;
//	}
//
//	public void setSignatures(Collection<Signature> signatures) {
//		this.signatures = signatures;
//	}

	public TypeConv getTypeConvention() {
		return typeConvention;
	}

	public void setTypeConvention(TypeConv typeConvention) {
		this.typeConvention = typeConvention;
	}

	public Administrateur getAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}

	@Override
	public String toString() {
		return "Convention{" +
				"idConv=" + idConv +
				", dateEdition=" + dateEdition +
				", objetConv='" + objetConv + '\'' +
				", dateEntreeVigueur=" + dateEntreeVigueur +
				", dateExpiration=" + dateExpiration +
				", typeConvention=" + typeConvention +
				", administrateur=" + administrateur +
				'}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((administrateur == null) ? 0 : administrateur.hashCode());
		result = prime * result + ((dateEdition == null) ? 0 : dateEdition.hashCode());
		result = prime * result + ((dateEntreeVigueur == null) ? 0 : dateEntreeVigueur.hashCode());
		result = prime * result + ((dateExpiration == null) ? 0 : dateExpiration.hashCode());
		result = prime * result + ((idConv == null) ? 0 : idConv.hashCode());
		result = prime * result + ((objetConv == null) ? 0 : objetConv.hashCode());
		result = prime * result + ((typeConvention == null) ? 0 : typeConvention.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Convention other = (Convention) obj;
		if (administrateur == null) {
			if (other.administrateur != null)
				return false;
		} else if (!administrateur.equals(other.administrateur))
			return false;
		if (dateEdition == null) {
			if (other.dateEdition != null)
				return false;
		} else if (!dateEdition.equals(other.dateEdition))
			return false;
		if (dateEntreeVigueur == null) {
			if (other.dateEntreeVigueur != null)
				return false;
		} else if (!dateEntreeVigueur.equals(other.dateEntreeVigueur))
			return false;
		if (dateExpiration == null) {
			if (other.dateExpiration != null)
				return false;
		} else if (!dateExpiration.equals(other.dateExpiration))
			return false;
		if (idConv == null) {
			if (other.idConv != null)
				return false;
		} else if (!idConv.equals(other.idConv))
			return false;
		if (objetConv == null) {
			if (other.objetConv != null)
				return false;
		} else if (!objetConv.equals(other.objetConv))
			return false;
		if (typeConvention == null) {
			if (other.typeConvention != null)
				return false;
		} else if (!typeConvention.equals(other.typeConvention))
			return false;
		return true;
	}

}
