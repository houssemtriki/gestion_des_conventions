package com.examen.j2ee.examen.entiter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ConventionForm {

    private Long idConv;
    private String dateEdition;
    private String dateEntreeVigueur;
    private String dateExpiration;
    private String objetConv;
    private Long participant1;
    private String dateSignature1;
    private Long participant2;
    private String dateSignature2;
    private Long participant3;
    private String dateSignature3;
    private Long participant4;
    private String dateSignature4;
    private Long typeConvention;

    public ConventionForm(){

    }
    public ConventionForm(Convention convention, List<Signature> signatures){
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        this.idConv = convention.getIdConv();
        this.dateEdition = formatter.format(convention.getDateEdition());
        this.dateEntreeVigueur = formatter.format(convention.getDateEntreeVigueur());
        this.dateExpiration = formatter.format(convention.getDateExpiration());
        this.objetConv = convention.getObjetConv();
        this.typeConvention = convention.getTypeConvention().getIdTypConv();
        this.participant1 = signatures.get(0).getParticipant().getIdParticipant();
        this.dateSignature1 = formatter.format(signatures.get(0).getDateSignature());
        this.participant2 = signatures.get(1).getParticipant().getIdParticipant();
        this.dateSignature2 = formatter.format(signatures.get(1).getDateSignature());
        if(signatures.size()>=3)
        {
            this.participant3 = signatures.get(2).getParticipant().getIdParticipant();
            this.dateSignature3 = formatter.format(signatures.get(2).getDateSignature());
        }
        if(signatures.size()==4)
        {
            this.participant4 = signatures.get(3).getParticipant().getIdParticipant();
            this.dateSignature4 = formatter.format(signatures.get(3).getDateSignature());
        }

    }

    public Long getIdConv() {
        return idConv;
    }

    public void setIdConv(Long idConv) {
        this.idConv = idConv;
    }

    public String getDateEdition() {
        return dateEdition;
    }

    public void setDateEdition(String dateEdition) {
        this.dateEdition = dateEdition;
    }

    public String getDateEntreeVigueur() {
        return dateEntreeVigueur;
    }

    public void setDateEntreeVigueur(String dateEntreeVigueur) {
        this.dateEntreeVigueur = dateEntreeVigueur;
    }

    public String getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(String dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getObjetConv() {
        return objetConv;
    }

    public void setObjetConv(String objetConv) {
        this.objetConv = objetConv;
    }

    public Long getParticipant1() {
        return participant1;
    }

    public void setParticipant1(Long participant1) {
        this.participant1 = participant1;
    }

    public String getDateSignature1() {
        return dateSignature1;
    }

    public void setDateSignature1(String dateSignature1) {
        this.dateSignature1 = dateSignature1;
    }

    public Long getParticipant2() {
        return participant2;
    }

    public void setParticipant2(Long participant2) {
        this.participant2 = participant2;
    }

    public String getDateSignature2() {
        return dateSignature2;
    }

    public void setDateSignature2(String dateSignature2) {
        this.dateSignature2 = dateSignature2;
    }

    public Long getParticipant3() {
        return participant3;
    }

    public void setParticipant3(Long participant3) {
        this.participant3 = participant3;
    }

    public String getDateSignature3() {
        return dateSignature3;
    }

    public void setDateSignature3(String dateSignature3) {
        this.dateSignature3 = dateSignature3;
    }

    public Long getParticipant4() {
        return participant4;
    }

    public void setParticipant4(Long participant4) {
        this.participant4 = participant4;
    }

    public String getDateSignature4() {
        return dateSignature4;
    }

    public void setDateSignature4(String dateSignature4) {
        this.dateSignature4 = dateSignature4;
    }

    public Long getTypeConvention() {
        return typeConvention;
    }

    public void setTypeConvention(Long typeConvention) {
        this.typeConvention = typeConvention;
    }

    @Override
    public String toString() {
        return "ConventionForm{" +
                "dateEdition='" + dateEdition + '\'' +
                ", dateEntreeVigueur='" + dateEntreeVigueur + '\'' +
                ", dateExpiration='" + dateExpiration + '\'' +
                ", objetConv='" + objetConv + '\'' +
                ", participant1=" + participant1 +
                ", dateSignature1='" + dateSignature1 + '\'' +
                ", participant2=" + participant2 +
                ", dateSignature2='" + dateSignature2 + '\'' +
                ", participant3=" + participant3 +
                ", dateSignature3='" + dateSignature3 + '\'' +
                ", participant4=" + participant4 +
                ", dateSignature4='" + dateSignature4 + '\'' +
                ", typeConvention=" + typeConvention +
                '}';
    }
}
