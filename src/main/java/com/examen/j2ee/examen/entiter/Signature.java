package com.examen.j2ee.examen.entiter;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

@Entity
public class Signature implements Serializable {

	@Id
	@GeneratedValue()
	private Long id;
	private Date dateSignature;
	@ManyToOne
	@JoinColumn(name = "id_par_fk", referencedColumnName = "idParticipant" )
	private Participant participant;
	@ManyToOne
	@JoinColumn(name = "id_con_fk", referencedColumnName = "idConv")
	private Convention convention;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Signature{" +
				"dateSignature=" + dateSignature +
				", participant=" + participant +
				", convention=" + convention +
				'}';
	}

	public Signature() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Signature(Date dateSignature, Participant participant, Convention convention) {
		this.dateSignature = dateSignature;
		this.participant = participant;
		this.convention = convention;
	}

	public Signature(String date,Participant participant,Convention convention) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		this.dateSignature = formatter.parse(date);
		this.participant = participant;
		this.convention = convention;
	}

	public Date getDateSignature() {
		return dateSignature;
	}

	public void setDateSignature(Date dateSignature) {
		this.dateSignature = dateSignature;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Convention getConvention() {
		return convention;
	}

	public void setConvention(Convention convention) {
		this.convention = convention;
	}
}
