/**
 * 
 */
$('document').ready(function(){	
	$('.table #editButton').on('click',function(event){	
		
		event.preventDefault();
		
		var href = $(this).attr('href');
		
		$.get(href, function(participant, status){
			$('#idParticipantEdit').val(participant.idParticipant);
			$('#designationEdit').val(participant.designation);
		});
		
		$('#editModal').modal();				
	});
	

	$('.table #deleteButton').on('click',function(event){
		event.preventDefault();		
		var href = $(this).attr('href');
		$('#deleteModal #delRef').attr('href', href);		
		$('#deleteModal').modal();
	});
});