/**
 * 
 */
$('document').ready(function(){	
	$('.table #editButton').on('click',function(event){	
		
		event.preventDefault();
		var href = $(this).attr('href');

		$.get(href, function(conventionForm, status){
			console.log(conventionForm);
			$('#idConvEdit').val(conventionForm.idConv);
			$('#dateEditionEdit').val(conventionForm.dateEdition);
			$('#dateEntreeVigueurEdit').val(conventionForm.dateEntreeVigueur);
			$('#dateExpirationEdit').val(conventionForm.dateExpiration);
			$('#objetConvEdit').val(conventionForm.objetConv);
			$('#participant1Edit').val(conventionForm.participant1);
			$('#dateSignature1Edit').val(conventionForm.dateSignature1);
			$('#participant2Edit').val(conventionForm.participant2);
			$('#dateSignature2Edit').val(conventionForm.dateSignature3);
			$('#participant3Edit').val(conventionForm.participant3);
			$('#dateSignature3Edit').val(conventionForm.dateSignature3);
			$('#participant4Edit').val(conventionForm.participant4);
			$('#dateSignature4Edit').val(conventionForm.dateSignature4);

			$('#typeConventionEdit').val(conventionForm.typeConvention);
		});
		
		$('#editModal').modal();				
	});
	

	$('.table #deleteButton').on('click',function(event){
		event.preventDefault();		
		var href = $(this).attr('href');
		$('#deleteModal #delRef').attr('href', href);		
		$('#deleteModal').modal();
	});

});