FROM openjdk:8
COPY target/examen-0.0.1-SNAPSHOT.jar examen-0.0.1-SNAPSHOT.jar
EXPOSE 8086
ENTRYPOINT ["java","-jar","examen-0.0.1-SNAPSHOT.jar"]